/*
 Navicat Premium Data Transfer

 Source Server         : 腾讯云数据库
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 101.35.29.132:3306
 Source Schema         : ruoyi-staging

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 09/08/2022 21:39:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cms_article
-- ----------------------------
DROP TABLE IF EXISTS `cms_article`;
CREATE TABLE `cms_article`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) NULL DEFAULT 0 COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT 0 COMMENT '部门ID',
  `category_id` bigint(20) NULL DEFAULT 0 COMMENT '分类ID',
  `article_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '标题',
  `article_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '封面图',
  `article_keyword` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '关键字',
  `article_brief` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '简介',
  `article_hit` int(8) NULL DEFAULT 0 COMMENT '点击次数',
  `article_detail` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '详情',
  `article_from` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '来源',
  `article_author` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '作者',
  `publish_time` datetime(0) NULL DEFAULT NULL COMMENT '发布时间',
  `category_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '分类名称',
  `sort` int(8) NULL DEFAULT 0 COMMENT '排序',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态(0未删除 1已删除)',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '数据状态(0正常 1停用)',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文章表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cms_article
-- ----------------------------
INSERT INTO `cms_article` VALUES (1, 1, 103, 2, '测试文章', 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/f5ec79ee9efa44b09e95f20147edefdf.jpg', '关键字测试', '简介测试测试', 0, '<p>文章详情测试测试文章详情测试测试文章详情测试测试文章详情测试测试文章详情测试测试文章详情测试测试文章详情测试测试文章详情测试测试文章详情测试测试文章详情测试测试文章详情测试测试文章详情测试测试文章详情测试测试文章详情测试测试</p>', '网络', 'zouhuu', NULL, '二级分类1', 1, 0, 0, '', '2022-08-04 16:28:11', '', NULL, '');
INSERT INTO `cms_article` VALUES (2, 1, 103, 4, '测试文章2', 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/f5ec79ee9efa44b09e95f20147edefdf.jpg', '关键字', '简介简介简介简介', 0, '<p>内容内容内容内容</p>', '网络', 'zouhuu', NULL, '二级分类2', 0, 0, 0, 'admin', '2022-08-04 17:05:13', 'admin', '2022-08-07 17:59:40', '');
INSERT INTO `cms_article` VALUES (3, 1, 103, 4, '测试文章3', 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/f5ec79ee9efa44b09e95f20147edefdf.jpg', '关键字', '简介简介简介简介', 0, '<p>内容内容内容内容</p>', '网络', 'zouhuu', NULL, '二级分类2', 0, 0, 0, 'admin', '2022-08-04 17:05:13', 'admin', '2022-08-04 17:11:55', '');
INSERT INTO `cms_article` VALUES (4, 1, 103, 4, '测试文章4', 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/f5ec79ee9efa44b09e95f20147edefdf.jpg', '关键字', '简介简介简介简介', 0, '<p>内容内容内容内容</p>', '网络', 'zouhuu', NULL, '二级分类2', 0, 0, 0, 'admin', '2022-08-04 17:05:13', 'admin', '2022-08-04 17:11:55', '');
INSERT INTO `cms_article` VALUES (5, 1, 103, 4, '测试文章5', 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/f5ec79ee9efa44b09e95f20147edefdf.jpg', '关键字', '简介简介简介简介', 0, '<p>内容内容内容内容</p>', '网络', 'zouhuu', NULL, '二级分类2', 0, 0, 0, 'admin', '2022-08-04 17:05:13', 'admin', '2022-08-04 17:11:55', '');
INSERT INTO `cms_article` VALUES (6, 1, 103, 4, '测试文章6', 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/f5ec79ee9efa44b09e95f20147edefdf.jpg', '关键字', '简介简介简介简介', 0, '<p>内容内容内容内容</p>', '网络', 'zouhuu', NULL, '二级分类2', 0, 0, 0, 'admin', '2022-08-04 17:05:13', 'admin', '2022-08-04 17:11:55', '');
INSERT INTO `cms_article` VALUES (7, 1, 103, 4, '测试文章7', 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/f5ec79ee9efa44b09e95f20147edefdf.jpg', '关键字', '简介简介简介简介', 0, '<p>内容内容内容内容</p>', '网络', 'zouhuu', NULL, '二级分类2', 0, 0, 0, 'admin', '2022-08-04 17:05:13', 'admin', '2022-08-04 17:11:55', '');
INSERT INTO `cms_article` VALUES (8, 1, 103, 4, '测试文章8', 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/f5ec79ee9efa44b09e95f20147edefdf.jpg', '关键字', '简介简介简介简介', 0, '<p>内容内容内容内容</p>', '网络', 'zouhuu', NULL, '二级分类2', 0, 0, 0, 'admin', '2022-08-04 17:05:13', 'admin', '2022-08-04 17:11:55', '');

-- ----------------------------
-- Table structure for cms_article_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_article_category`;
CREATE TABLE `cms_article_category`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父ID',
  `user_id` bigint(20) NULL DEFAULT 0 COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT 0 COMMENT '部门ID',
  `category_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '分类名称',
  `category_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '分类图片',
  `category_keyword` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '分类关键字',
  `sort` int(8) NULL DEFAULT 0 COMMENT '排序',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态(0未删除 1已删除)',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '数据状态(0正常 1停用)',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文章分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cms_article_category
-- ----------------------------
INSERT INTO `cms_article_category` VALUES (1, 0, 0, 0, '一级分类1', '', '', 0, 0, 0, '', '2022-08-04 12:59:32', '', '2022-08-04 15:42:00', '');
INSERT INTO `cms_article_category` VALUES (2, 1, 0, 0, '二级分类1', '', '', 0, 0, 0, '', '2022-08-04 13:05:36', '', '2022-08-04 15:42:06', '');
INSERT INTO `cms_article_category` VALUES (3, 0, 0, 0, '一级分类2', '', '', 0, 0, 0, '', '2022-08-04 15:42:16', '', NULL, '');
INSERT INTO `cms_article_category` VALUES (4, 3, 0, 0, '二级分类2', '', '', 1, 0, 0, '', '2022-08-04 15:42:26', '', NULL, '');
INSERT INTO `cms_article_category` VALUES (5, 3, 0, 0, '二级分类3', '', '', 0, 0, 0, '', '2022-08-04 15:42:37', '', NULL, '');

-- ----------------------------
-- Table structure for cms_document
-- ----------------------------
DROP TABLE IF EXISTS `cms_document`;
CREATE TABLE `cms_document`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) NULL DEFAULT 0 COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT 0 COMMENT '部门ID',
  `doc_type` tinyint(4) NULL DEFAULT 0 COMMENT '文档类型(0其他 1协议 2资料)',
  `doc_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文档标题',
  `doc_brief` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文档简介',
  `doc_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文档图片',
  `doc_detail` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容详情',
  `doc_attach` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文档附件',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态(0未删除 1已删除)',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '数据状态(0正常 1停用)',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '平台相关文档' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cms_document
-- ----------------------------
INSERT INTO `cms_document` VALUES (1, 1, 103, 1, '平台协议', '平台协议简介', 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/efac83d0c4674e9ba0a8da4bbdf0dcc6.jpg', '<p>平台协议简介平台协议简介平台协议简介平台协议简介平台协议简介平台协议简介</p>', '', 0, 0, 'admin', '2022-08-04 17:33:07', '', NULL, '');
INSERT INTO `cms_document` VALUES (2, 1, 103, 0, '关于我们', '', '', '<ol><li><strong>12312</strong></li><li><br></li></ol><p>关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们</p>', '', 0, 0, 'admin', '2022-08-06 20:40:27', 'admin', '2022-08-06 20:48:16', '');
INSERT INTO `cms_document` VALUES (3, 1, 103, 0, '关于我们', '', '', '<p>关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们</p>', '', 0, 0, 'admin', '2022-08-06 20:40:18', '', NULL, '');

-- ----------------------------
-- Table structure for cms_feedback
-- ----------------------------
DROP TABLE IF EXISTS `cms_feedback`;
CREATE TABLE `cms_feedback`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` bigint(20) NULL DEFAULT 0 COMMENT '用户ID',
  `user_id` bigint(20) NULL DEFAULT 0 COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT 0 COMMENT '部门ID',
  `contact_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '联系人',
  `contact_phone` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '联系电话',
  `feedback_type` tinyint(4) NULL DEFAULT 0 COMMENT '反馈类型(0功能异常 1产品建议 2违规举报)',
  `feedback_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '反馈标题',
  `feedback_content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '反馈内容',
  `feedback_attach` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '反馈附件',
  `handle_level` tinyint(4) NULL DEFAULT 0 COMMENT '处理等级(0普通 1加急 2紧急)',
  `reply_content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '回复内容',
  `reply_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '回复人',
  `reply_time` datetime(0) NULL DEFAULT NULL COMMENT '回复时间',
  `feedback_status` tinyint(4) NULL DEFAULT 0 COMMENT '反馈状态(0待处理 1处理中 2已完成)',
  `com_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '评价',
  `com_grade` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '评分',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态(0未删除 1已删除)',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '数据状态(0正常 1停用)',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户反馈表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cms_feedback
-- ----------------------------
INSERT INTO `cms_feedback` VALUES (1, 1, 1, 103, '邹虎', '18273881083', 0, '无法打开小程序', '无法打开小程序', '', 1, '已修复问题', 'admin', '2022-08-04 23:20:30', 2, '', 0.00, 0, 0, 'admin', '2022-08-04 22:32:32', 'admin', '2022-08-04 23:20:30', '');
INSERT INTO `cms_feedback` VALUES (2, 1, 0, 0, '邹虎', '18273881083', 0, '无法打开小程序', '测试无法打开小程序测试测试', 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/a09ae33bbd4f4e65b1e5a3998ce83e8f.jpg,https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/25234c9352284628b987cfb00dfa5b77.jpg', 0, '', '', NULL, 0, '', 0.00, 0, 0, '', '2022-08-05 23:47:14', '', NULL, '');
INSERT INTO `cms_feedback` VALUES (3, 1, 0, 0, '邹虎', '18273881083', 0, '无法打开小程序', '测试测试测试出色处测试测试', '', 0, '', '', NULL, 0, '', 0.00, 0, 0, '', '2022-08-05 23:50:37', '', NULL, '');
INSERT INTO `cms_feedback` VALUES (4, 1, 0, 0, 'zouhuu', '18273881083', 0, '无法打开小程序无法打开小程序', '测试测试测试出色处色', '', 0, '', '', NULL, 0, '', 0.00, 0, 0, '', '2022-08-05 23:51:05', '', NULL, '');
INSERT INTO `cms_feedback` VALUES (5, 1, 0, 0, '邹虎', '18273881083', 0, '无法打开小程序无法打开小程序', '测试测试彩色车测试测试', '', 0, '', '', NULL, 0, '', 0.00, 1, 0, '', '2022-08-05 23:54:40', '', NULL, '');
INSERT INTO `cms_feedback` VALUES (6, 1, 0, 0, '邹虎', '18273881083', 1, '产品建议', '测设测试测试测试测试aaa', 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/a09ae33bbd4f4e65b1e5a3998ce83e8f.jpg,https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/25234c9352284628b987cfb00dfa5b77.jpg', 0, '', '', NULL, 0, '', 0.00, 0, 0, '', '2022-08-05 23:57:02', '', '2022-08-06 18:58:01', '');

-- ----------------------------
-- Table structure for cms_help
-- ----------------------------
DROP TABLE IF EXISTS `cms_help`;
CREATE TABLE `cms_help`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) NULL DEFAULT 0 COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT 0 COMMENT '部门ID',
  `category_id` bigint(20) NULL DEFAULT 0 COMMENT '分类ID',
  `help_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '题目',
  `help_brief` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '简介',
  `help_detail` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '详情',
  `help_hit` int(8) NULL DEFAULT 0 COMMENT '点击次数',
  `category_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '分类名称',
  `sort` int(8) NULL DEFAULT 0 COMMENT '排序',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态(0未删除 1已删除)',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '数据状态(0正常 1停用)',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '帮助文档表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cms_help
-- ----------------------------
INSERT INTO `cms_help` VALUES (1, 1, 103, 1, '常见问题1', '常见问题简介', '<p>常见问题详情常见问题详情常见问题详情常见问题详情</p>', 0, '常见问题', 1, 0, 0, 'admin', '2022-08-04 17:24:33', 'admin', '2022-08-04 17:28:29', '');
INSERT INTO `cms_help` VALUES (2, 1, 103, 1, '常见问题2', '常见问题2常见问题2常见问题2', '<p>常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2常见问题2</p>', 0, '常见问题', 0, 0, 0, 'admin', '2022-08-07 10:31:47', '', NULL, '');

-- ----------------------------
-- Table structure for cms_help_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_help_category`;
CREATE TABLE `cms_help_category`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) NULL DEFAULT 0 COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT 0 COMMENT '部门ID',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '分类名称',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类图标',
  `brief` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类简介',
  `sort` int(8) NULL DEFAULT 0 COMMENT '排序',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态(0未删除 1已删除)',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '数据状态(0正常 1停用)',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '帮助文档分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cms_help_category
-- ----------------------------
INSERT INTO `cms_help_category` VALUES (1, 1, 103, '常见问题', 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/c047efe48ff44308bc417e5dd8c96e52.jpg', '常见问题简介简介', 1, 0, 0, 'admin', '2022-08-04 17:14:37', '', NULL, '');
INSERT INTO `cms_help_category` VALUES (2, 1, 103, '帮助文档', 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/a37cd6124eb549419d9c40a9f66d2f60.jpg', NULL, 0, 0, 0, 'admin', '2022-08-04 17:27:16', 'admin', '2022-08-04 17:27:26', '');

-- ----------------------------
-- Table structure for cms_message
-- ----------------------------
DROP TABLE IF EXISTS `cms_message`;
CREATE TABLE `cms_message`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` bigint(20) NULL DEFAULT 0 COMMENT '用户ID',
  `user_id` bigint(20) NULL DEFAULT 0 COMMENT '系统用户ID',
  `dept_id` bigint(20) NULL DEFAULT 0 COMMENT '系统部门ID',
  `msg_object` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '消息对象(client客户 staff员工)',
  `msg_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '消息类型',
  `msg_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '消息标题',
  `msg_content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '消息内容',
  `msg_attach` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '消息附件',
  `send_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '发送人',
  `send_time` datetime(0) NULL DEFAULT NULL COMMENT '发送时间',
  `read_time` datetime(0) NULL DEFAULT NULL COMMENT '阅读时间',
  `is_read` tinyint(4) NULL DEFAULT 0 COMMENT '是否阅读(0否 1是)',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态(0未删除 1已删除)',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '数据状态(0正常 1停用)',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (3, 'sys_oss', '对象存储表', NULL, NULL, 'SysOss', 'crud', 'com.ruoyi.system', 'system', 'sysOss', '对象存储', 'zouhuu', '0', '/', NULL, 'admin', '2022-07-25 16:20:35', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (4, 'ums_user', '用户表', NULL, NULL, 'UmsUser', 'crud', 'com.ruoyi.ums', 'ums', 'umsUser', '用户管理', 'zouhuu', '0', '/', '{}', 'admin', '2022-08-01 17:19:26', '', '2022-08-01 17:21:36', NULL);
INSERT INTO `gen_table` VALUES (5, 'sys_group_category', '组合数据分类表', NULL, NULL, 'SysGroupCategory', 'crud', 'com.ruoyi.system', 'system', 'sysGroupCategory', '组合数据分类', 'zouhuu', '0', '/', '{\"parentMenuId\":1}', 'admin', '2022-08-01 23:19:11', '', '2022-08-01 23:21:54', NULL);
INSERT INTO `gen_table` VALUES (6, 'sys_group_data', '组合数据表', NULL, NULL, 'SysGroupData', 'crud', 'com.ruoyi.system', 'system', 'sysGroupData', '组合数据', 'zouhuu', '0', '/', '{\"parentMenuId\":1}', 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:22:01', NULL);
INSERT INTO `gen_table` VALUES (7, 'cms_article', '文章表', NULL, NULL, 'CmsArticle', 'crud', 'com.ruoyi.cms', 'cms', 'cmsArticle', '文章', 'zouhuu', '0', '/', '{\"parentMenuId\":\"2038\"}', 'admin', '2022-08-04 10:57:00', '', '2022-08-04 16:21:31', NULL);
INSERT INTO `gen_table` VALUES (8, 'cms_article_category', '文章分类表', '', '', 'CmsArticleCategory', 'tree', 'com.ruoyi.cms', 'cms', 'cmsArticleCategory', '文章分类', 'zouhuu', '0', '/', '{\"treeCode\":\"id\",\"treeName\":\"category_name\",\"treeParentCode\":\"parent_id\",\"parentMenuId\":2038}', 'admin', '2022-08-04 10:57:02', '', '2022-08-04 11:00:24', NULL);
INSERT INTO `gen_table` VALUES (9, 'cms_document', '平台相关文档', NULL, NULL, 'CmsDocument', 'crud', 'com.ruoyi.cms', 'cms', 'cmsDocument', '平台相关文档', 'zouhuu', '0', '/', '{\"parentMenuId\":2038}', 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:01:15', NULL);
INSERT INTO `gen_table` VALUES (10, 'cms_feedback', '用户反馈表', NULL, NULL, 'CmsFeedback', 'crud', 'com.ruoyi.cms', 'cms', 'cmsFeedback', '用户反馈', 'zouhuu', '0', '/', '{\"parentMenuId\":2038}', 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:02:52', NULL);
INSERT INTO `gen_table` VALUES (11, 'cms_help', '帮助文档表', NULL, NULL, 'CmsHelp', 'crud', 'com.ruoyi.cms', 'cms', 'cmsHelp', '帮助文档', 'zouhuu', '0', '/', '{\"parentMenuId\":2038}', 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:03:47', NULL);
INSERT INTO `gen_table` VALUES (12, 'cms_help_category', '帮助文档分类表', NULL, NULL, 'CmsHelpCategory', 'crud', 'com.ruoyi.cms', 'cms', 'cmsHelpCategory', '帮助文档分类', 'zouhuu', '0', '/', '{\"parentMenuId\":2038}', 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:04:10', NULL);
INSERT INTO `gen_table` VALUES (13, 'cms_message', '消息表', NULL, NULL, 'CmsMessage', 'crud', 'com.ruoyi.cms', 'cms', 'cmsMessage', '消息', 'zouhuu', '0', '/', '{\"parentMenuId\":2038}', 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:05:30', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 232 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (33, '3', 'oss_id', 'ID', 'bigint(20) unsigned', 'Long', 'ossId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-07-25 16:20:35', '', NULL);
INSERT INTO `gen_table_column` VALUES (34, '3', 'file_name', '文件名', 'varchar(255)', 'String', 'fileName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-07-25 16:20:35', '', NULL);
INSERT INTO `gen_table_column` VALUES (35, '3', 'original_name', '原文件名', 'varchar(255)', 'String', 'originalName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2022-07-25 16:20:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (36, '3', 'file_suffix', '文件后缀', 'varchar(20)', 'String', 'fileSuffix', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-07-25 16:20:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (37, '3', 'file_size', '文件大小', 'bigint(20)', 'Long', 'fileSize', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-07-25 16:20:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (38, '3', 'url', 'URL地址', 'varchar(500)', 'String', 'url', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 6, 'admin', '2022-07-25 16:20:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (39, '3', 'source', '来源(service后台 client客户端)', 'varchar(20)', 'String', 'source', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-07-25 16:20:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (40, '3', 'service', '服务商(aliyun阿里云 qiniu七牛云)', 'varchar(20)', 'String', 'service', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-07-25 16:20:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (41, '3', 'del_flag', '删除状态', 'tinyint(4)', 'Integer', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 9, 'admin', '2022-07-25 16:20:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (42, '3', 'status', '数据状态', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 10, 'admin', '2022-07-25 16:20:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (43, '3', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2022-07-25 16:20:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (44, '3', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 12, 'admin', '2022-07-25 16:20:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (45, '3', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 13, 'admin', '2022-07-25 16:20:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (46, '3', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 14, 'admin', '2022-07-25 16:20:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (47, '4', 'id', 'id', 'bigint(20) unsigned', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:36');
INSERT INTO `gen_table_column` VALUES (48, '4', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:36');
INSERT INTO `gen_table_column` VALUES (49, '4', 'dept_id', '部门ID', 'bigint(20)', 'Long', 'deptId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:36');
INSERT INTO `gen_table_column` VALUES (50, '4', 'wx_client_openid', '微信公众号客户端openid', 'varchar(255)', 'String', 'wxClientOpenid', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:36');
INSERT INTO `gen_table_column` VALUES (51, '4', 'wxm_client_openid', '微信小程序客户端openid', 'varchar(255)', 'String', 'wxmClientOpenid', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:36');
INSERT INTO `gen_table_column` VALUES (52, '4', 'wxm_staff_openid', '微信小程序员工端openid', 'varchar(255)', 'String', 'wxmStaffOpenid', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:36');
INSERT INTO `gen_table_column` VALUES (53, '4', 'wx_unionid', '微信unionid', 'varchar(255)', 'String', 'wxUnionid', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (54, '4', 'username', '用户名', 'varchar(50)', 'String', 'username', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 8, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (55, '4', 'password', '密码', 'varchar(255)', 'String', 'password', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (56, '4', 'nickname', '昵称', 'varchar(100)', 'String', 'nickname', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 10, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (57, '4', 'avatar', '用户头像', 'varchar(255)', 'String', 'avatar', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (58, '4', 'phone', '手机号码', 'varchar(20)', 'String', 'phone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (59, '4', 'eamil', '邮箱', 'varchar(50)', 'String', 'eamil', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (60, '4', 'idcard', '身份证号', 'varchar(100)', 'String', 'idcard', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (61, '4', 'login_ip', '登录IP', 'varchar(255)', 'String', 'loginIp', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2022-08-01 17:19:27', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (62, '4', 'login_type', '登录终端类型(minapp小程序 app APP)', 'varchar(32)', 'String', 'loginType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 16, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (63, '4', 'login_time', '登录时间', 'datetime', 'Date', 'loginTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 17, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (64, '4', 'last_login_ip', '上次登录IP', 'varchar(255)', 'String', 'lastLoginIp', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 18, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (65, '4', 'last_login_type', '上次登录终端类型', 'varchar(32)', 'String', 'lastLoginType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 19, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (66, '4', 'last_login_time', '上次登录终端时间', 'datetime', 'Date', 'lastLoginTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 20, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (67, '4', 'login_num', '登录次数', 'int(11)', 'Long', 'loginNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 21, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:37');
INSERT INTO `gen_table_column` VALUES (68, '4', 'realname', '真实姓名', 'varchar(32)', 'String', 'realname', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 22, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:38');
INSERT INTO `gen_table_column` VALUES (69, '4', 'sex', '性别(0未知 1男 2女)', 'tinyint(4)', 'Integer', 'sex', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'sys_user_sex', 23, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:38');
INSERT INTO `gen_table_column` VALUES (70, '4', 'birthday', '出生日期', 'datetime', 'Date', 'birthday', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 24, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:38');
INSERT INTO `gen_table_column` VALUES (71, '4', 'integral', '积分', 'decimal(8,2)', 'BigDecimal', 'integral', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 25, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:38');
INSERT INTO `gen_table_column` VALUES (72, '4', 'balance', '余额', 'decimal(8,2)', 'BigDecimal', 'balance', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 26, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:38');
INSERT INTO `gen_table_column` VALUES (73, '4', 'reg_time', '注册时间', 'datetime', 'Date', 'regTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 27, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:38');
INSERT INTO `gen_table_column` VALUES (74, '4', 'del_flag', '删除状态(0未删除 1已删除)', 'tinyint(4)', 'Integer', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'radio', 'common_del_status', 28, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:38');
INSERT INTO `gen_table_column` VALUES (75, '4', 'status', '数据状态(0正常 1停用)', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'common_data_status', 29, 'admin', '2022-08-01 17:19:28', '', '2022-08-01 17:21:38');
INSERT INTO `gen_table_column` VALUES (76, '4', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 30, 'admin', '2022-08-01 17:19:29', '', '2022-08-01 17:21:38');
INSERT INTO `gen_table_column` VALUES (77, '4', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 31, 'admin', '2022-08-01 17:19:29', '', '2022-08-01 17:21:38');
INSERT INTO `gen_table_column` VALUES (78, '4', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 32, 'admin', '2022-08-01 17:19:29', '', '2022-08-01 17:21:38');
INSERT INTO `gen_table_column` VALUES (79, '4', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 33, 'admin', '2022-08-01 17:19:29', '', '2022-08-01 17:21:38');
INSERT INTO `gen_table_column` VALUES (80, '4', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 34, 'admin', '2022-08-01 17:19:29', '', '2022-08-01 17:21:38');
INSERT INTO `gen_table_column` VALUES (81, '5', 'id', 'ID', 'bigint(20) unsigned', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-08-01 23:19:11', '', '2022-08-01 23:21:54');
INSERT INTO `gen_table_column` VALUES (82, '5', 'category_name', '数据组名称', 'varchar(100)', 'String', 'categoryName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-08-01 23:19:11', '', '2022-08-01 23:21:54');
INSERT INTO `gen_table_column` VALUES (83, '5', 'category_info', '数据提示', 'varchar(255)', 'String', 'categoryInfo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-08-01 23:19:11', '', '2022-08-01 23:21:54');
INSERT INTO `gen_table_column` VALUES (84, '5', 'category_key', '数据字段', 'varchar(63)', 'String', 'categoryKey', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:21:54');
INSERT INTO `gen_table_column` VALUES (85, '5', 'content', '数据组字段', 'text', 'String', 'content', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'editor', '', 5, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:21:54');
INSERT INTO `gen_table_column` VALUES (86, '5', 'category_type', '分类类型(0总后台配置)', 'tinyint(4)', 'Integer', 'categoryType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 6, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:21:54');
INSERT INTO `gen_table_column` VALUES (87, '5', 'del_flag', '删除状态', 'tinyint(4)', 'Integer', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', 'common_del_status', 7, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:21:54');
INSERT INTO `gen_table_column` VALUES (88, '5', 'status', '数据状态', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'common_data_status', 8, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:21:54');
INSERT INTO `gen_table_column` VALUES (89, '5', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 9, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:21:54');
INSERT INTO `gen_table_column` VALUES (90, '5', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:21:54');
INSERT INTO `gen_table_column` VALUES (91, '5', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:21:54');
INSERT INTO `gen_table_column` VALUES (92, '5', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 12, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:21:54');
INSERT INTO `gen_table_column` VALUES (93, '6', 'id', 'ID', 'bigint(20) unsigned', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:22:01');
INSERT INTO `gen_table_column` VALUES (94, '6', 'category_id', '分类ID', 'bigint(20)', 'Long', 'categoryId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:22:01');
INSERT INTO `gen_table_column` VALUES (95, '6', 'group_value', '数据组值', 'text', 'String', 'groupValue', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 3, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:22:01');
INSERT INTO `gen_table_column` VALUES (96, '6', 'group_key', '数据字段', 'varchar(63)', 'String', 'groupKey', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:22:01');
INSERT INTO `gen_table_column` VALUES (97, '6', 'sort', '排序', 'int(4)', 'Integer', 'sort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-08-01 23:19:12', '', '2022-08-01 23:22:01');
INSERT INTO `gen_table_column` VALUES (98, '6', 'del_flag', '删除状态', 'tinyint(4)', 'Integer', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 6, 'admin', '2022-08-01 23:19:13', '', '2022-08-01 23:22:02');
INSERT INTO `gen_table_column` VALUES (99, '6', 'status', '数据状态', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'common_data_status', 7, 'admin', '2022-08-01 23:19:13', '', '2022-08-01 23:22:02');
INSERT INTO `gen_table_column` VALUES (100, '6', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2022-08-01 23:19:13', '', '2022-08-01 23:22:02');
INSERT INTO `gen_table_column` VALUES (101, '6', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-08-01 23:19:13', '', '2022-08-01 23:22:02');
INSERT INTO `gen_table_column` VALUES (102, '6', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2022-08-01 23:19:13', '', '2022-08-01 23:22:02');
INSERT INTO `gen_table_column` VALUES (103, '6', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2022-08-01 23:19:13', '', '2022-08-01 23:22:02');
INSERT INTO `gen_table_column` VALUES (104, '7', 'id', 'id', 'bigint(20) unsigned', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-08-04 10:57:00', '', '2022-08-04 16:21:31');
INSERT INTO `gen_table_column` VALUES (105, '7', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-08-04 10:57:00', '', '2022-08-04 16:21:31');
INSERT INTO `gen_table_column` VALUES (106, '7', 'dept_id', '部门ID', 'bigint(20)', 'Long', 'deptId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-08-04 10:57:00', '', '2022-08-04 16:21:31');
INSERT INTO `gen_table_column` VALUES (107, '7', 'category_id', '分类ID', 'bigint(20)', 'Long', 'categoryId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-08-04 10:57:00', '', '2022-08-04 16:21:31');
INSERT INTO `gen_table_column` VALUES (108, '7', 'article_title', '标题', 'varchar(100)', 'String', 'articleTitle', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:31');
INSERT INTO `gen_table_column` VALUES (109, '7', 'article_img', '封面图', 'varchar(255)', 'String', 'articleImg', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'imageUpload', '', 6, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:31');
INSERT INTO `gen_table_column` VALUES (110, '7', 'article_keyword', '关键字', 'varchar(100)', 'String', 'articleKeyword', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:31');
INSERT INTO `gen_table_column` VALUES (111, '7', 'article_brief', '简介', 'varchar(500)', 'String', 'articleBrief', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 8, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (112, '7', 'article_hit', '点击次数', 'int(8)', 'Integer', 'articleHit', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (113, '7', 'article_detail', '详情', 'longtext', 'String', 'articleDetail', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'editor', '', 10, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (114, '7', 'article_from', '来源', 'varchar(64)', 'String', 'articleFrom', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (115, '7', 'article_author', '作者', 'varchar(100)', 'String', 'articleAuthor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (116, '7', 'publish_time', '发布时间', 'datetime', 'Date', 'publishTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 13, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (117, '7', 'category_name', '分类名称', 'varchar(255)', 'String', 'categoryName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 14, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (118, '7', 'sort', '排序', 'int(8)', 'Integer', 'sort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (119, '7', 'del_flag', '删除状态(0未删除 1已删除)', 'tinyint(4)', 'Integer', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'radio', '', 16, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (120, '7', 'status', '数据状态(0正常 1停用)', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'common_data_status', 17, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (121, '7', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 18, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (122, '7', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 19, 'admin', '2022-08-04 10:57:01', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (123, '7', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 20, 'admin', '2022-08-04 10:57:02', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (124, '7', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 21, 'admin', '2022-08-04 10:57:02', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (125, '7', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 22, 'admin', '2022-08-04 10:57:02', '', '2022-08-04 16:21:32');
INSERT INTO `gen_table_column` VALUES (126, '8', 'id', 'id', 'bigint(20) unsigned', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-08-04 10:57:02', '', '2022-08-04 11:00:24');
INSERT INTO `gen_table_column` VALUES (127, '8', 'parent_id', '父ID', 'bigint(20)', 'Long', 'parentId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-08-04 10:57:02', '', '2022-08-04 11:00:24');
INSERT INTO `gen_table_column` VALUES (128, '8', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-08-04 10:57:02', '', '2022-08-04 11:00:24');
INSERT INTO `gen_table_column` VALUES (129, '8', 'dept_id', '部门ID', 'bigint(20)', 'Long', 'deptId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-08-04 10:57:02', '', '2022-08-04 11:00:24');
INSERT INTO `gen_table_column` VALUES (130, '8', 'category_name', '分类名称', 'varchar(255)', 'String', 'categoryName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2022-08-04 10:57:02', '', '2022-08-04 11:00:24');
INSERT INTO `gen_table_column` VALUES (131, '8', 'category_img', '分类图片', 'varchar(255)', 'String', 'categoryImg', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'imageUpload', '', 6, 'admin', '2022-08-04 10:57:02', '', '2022-08-04 11:00:24');
INSERT INTO `gen_table_column` VALUES (132, '8', 'category_keyword', '分类关键字', 'varchar(100)', 'String', 'categoryKeyword', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-08-04 10:57:02', '', '2022-08-04 11:00:24');
INSERT INTO `gen_table_column` VALUES (133, '8', 'sort', '排序', 'int(8)', 'Integer', 'sort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-08-04 10:57:02', '', '2022-08-04 11:00:24');
INSERT INTO `gen_table_column` VALUES (134, '8', 'del_flag', '删除状态(0未删除 1已删除)', 'tinyint(4)', 'Integer', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 9, 'admin', '2022-08-04 10:57:02', '', '2022-08-04 11:00:24');
INSERT INTO `gen_table_column` VALUES (135, '8', 'status', '数据状态(0正常 1停用)', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'common_data_status', 10, 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:00:24');
INSERT INTO `gen_table_column` VALUES (136, '8', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:00:24');
INSERT INTO `gen_table_column` VALUES (137, '8', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 12, 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:00:24');
INSERT INTO `gen_table_column` VALUES (138, '8', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 13, 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:00:25');
INSERT INTO `gen_table_column` VALUES (139, '8', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 14, 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:00:25');
INSERT INTO `gen_table_column` VALUES (140, '8', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 15, 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:00:25');
INSERT INTO `gen_table_column` VALUES (141, '9', 'id', 'id', 'bigint(20) unsigned', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:01:15');
INSERT INTO `gen_table_column` VALUES (142, '9', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:01:15');
INSERT INTO `gen_table_column` VALUES (143, '9', 'dept_id', '部门ID', 'bigint(20)', 'Long', 'deptId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:01:15');
INSERT INTO `gen_table_column` VALUES (144, '9', 'doc_type', '文档类型(0其他 1协议 2资料)', 'tinyint(4)', 'Integer', 'docType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'cms_doc_type', 4, 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:01:15');
INSERT INTO `gen_table_column` VALUES (145, '9', 'doc_title', '文档标题', 'varchar(100)', 'String', 'docTitle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:01:15');
INSERT INTO `gen_table_column` VALUES (146, '9', 'doc_brief', '文档简介', 'varchar(255)', 'String', 'docBrief', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:01:15');
INSERT INTO `gen_table_column` VALUES (147, '9', 'doc_img', '文档图片', 'varchar(255)', 'String', 'docImg', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'imageUpload', '', 7, 'admin', '2022-08-04 10:57:03', '', '2022-08-04 11:01:15');
INSERT INTO `gen_table_column` VALUES (148, '9', 'doc_detail', '内容详情', 'longtext', 'String', 'docDetail', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'editor', '', 8, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:01:15');
INSERT INTO `gen_table_column` VALUES (149, '9', 'doc_attach', '文档附件', 'varchar(1000)', 'String', 'docAttach', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'fileUpload', '', 9, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:01:15');
INSERT INTO `gen_table_column` VALUES (150, '9', 'del_flag', '删除状态(0未删除 1已删除)', 'tinyint(4)', 'Integer', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:01:15');
INSERT INTO `gen_table_column` VALUES (151, '9', 'status', '数据状态(0正常 1停用)', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'common_data_source', 11, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:01:16');
INSERT INTO `gen_table_column` VALUES (152, '9', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 12, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:01:16');
INSERT INTO `gen_table_column` VALUES (153, '9', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 13, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:01:16');
INSERT INTO `gen_table_column` VALUES (154, '9', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 14, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:01:16');
INSERT INTO `gen_table_column` VALUES (155, '9', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 15, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:01:16');
INSERT INTO `gen_table_column` VALUES (156, '9', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 16, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:01:16');
INSERT INTO `gen_table_column` VALUES (157, '10', 'id', 'id', 'bigint(20) unsigned', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:02:52');
INSERT INTO `gen_table_column` VALUES (158, '10', 'uid', '用户ID', 'bigint(20)', 'Long', 'uid', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:02:52');
INSERT INTO `gen_table_column` VALUES (159, '10', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:02:52');
INSERT INTO `gen_table_column` VALUES (160, '10', 'dept_id', '部门ID', 'bigint(20)', 'Long', 'deptId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (161, '10', 'contact_name', '联系人', 'varchar(64)', 'String', 'contactName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2022-08-04 10:57:04', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (162, '10', 'contact_phone', '联系电话', 'varchar(32)', 'String', 'contactPhone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (163, '10', 'feedback_type', '反馈类型(0功能异常 1产品建议 2违规举报)', 'tinyint(4)', 'Integer', 'feedbackType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'cms_feedback_type', 7, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (164, '10', 'feedback_title', '反馈标题', 'varchar(255)', 'String', 'feedbackTitle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (165, '10', 'feedback_content', '反馈内容', 'varchar(1000)', 'String', 'feedbackContent', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 9, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (166, '10', 'feedback_attach', '反馈附件', 'varchar(1000)', 'String', 'feedbackAttach', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'fileUpload', '', 10, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (167, '10', 'handle_level', '处理等级(0普通 1加急 2紧急)', 'tinyint(4)', 'Integer', 'handleLevel', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'cms_feedback_handle_level', 11, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (168, '10', 'reply_content', '回复内容', 'varchar(1000)', 'String', 'replyContent', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 12, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (169, '10', 'reply_by', '回复人', 'varchar(100)', 'String', 'replyBy', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (170, '10', 'reply_time', '回复时间', 'datetime', 'Date', 'replyTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 14, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (171, '10', 'feedback_status', '反馈状态(0待处理 1处理中 2已完成)', 'tinyint(4)', 'Integer', 'feedbackStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'cms_feedback_status', 15, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (172, '10', 'com_comment', '评价', 'varchar(500)', 'String', 'comComment', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 16, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (173, '10', 'com_grade', '评分', 'decimal(8,2)', 'BigDecimal', 'comGrade', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 17, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (174, '10', 'del_flag', '删除状态(0未删除 1已删除)', 'tinyint(4)', 'Integer', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 18, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (175, '10', 'status', '数据状态(0正常 1停用)', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'common_data_status', 19, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:53');
INSERT INTO `gen_table_column` VALUES (176, '10', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 20, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:54');
INSERT INTO `gen_table_column` VALUES (177, '10', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 21, 'admin', '2022-08-04 10:57:05', '', '2022-08-04 11:02:54');
INSERT INTO `gen_table_column` VALUES (178, '10', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 22, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:02:54');
INSERT INTO `gen_table_column` VALUES (179, '10', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 23, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:02:54');
INSERT INTO `gen_table_column` VALUES (180, '10', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 24, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:02:54');
INSERT INTO `gen_table_column` VALUES (181, '11', 'id', 'id', 'bigint(20) unsigned', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:03:47');
INSERT INTO `gen_table_column` VALUES (182, '11', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:03:47');
INSERT INTO `gen_table_column` VALUES (183, '11', 'dept_id', '部门ID', 'bigint(20)', 'Long', 'deptId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:03:47');
INSERT INTO `gen_table_column` VALUES (184, '11', 'category_id', '分类ID', 'bigint(20)', 'Long', 'categoryId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:03:47');
INSERT INTO `gen_table_column` VALUES (185, '11', 'help_title', '题目', 'varchar(100)', 'String', 'helpTitle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:03:47');
INSERT INTO `gen_table_column` VALUES (186, '11', 'help_brief', '简介', 'varchar(500)', 'String', 'helpBrief', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 6, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:03:48');
INSERT INTO `gen_table_column` VALUES (187, '11', 'help_detail', '详情', 'longtext', 'String', 'helpDetail', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'editor', '', 7, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:03:48');
INSERT INTO `gen_table_column` VALUES (188, '11', 'help_hit', '点击次数', 'int(8)', 'Integer', 'helpHit', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:03:48');
INSERT INTO `gen_table_column` VALUES (189, '11', 'category_name', '分类名称', 'varchar(100)', 'String', 'categoryName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 9, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:03:48');
INSERT INTO `gen_table_column` VALUES (190, '11', 'sort', '排序', 'int(8)', 'Integer', 'sort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:03:48');
INSERT INTO `gen_table_column` VALUES (191, '11', 'del_flag', '删除状态(0未删除 1已删除)', 'tinyint(4)', 'Integer', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2022-08-04 10:57:06', '', '2022-08-04 11:03:48');
INSERT INTO `gen_table_column` VALUES (192, '11', 'status', '数据状态(0正常 1停用)', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'common_data_status', 12, 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:03:48');
INSERT INTO `gen_table_column` VALUES (193, '11', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 13, 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:03:48');
INSERT INTO `gen_table_column` VALUES (194, '11', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 14, 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:03:48');
INSERT INTO `gen_table_column` VALUES (195, '11', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 15, 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:03:48');
INSERT INTO `gen_table_column` VALUES (196, '11', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 16, 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:03:48');
INSERT INTO `gen_table_column` VALUES (197, '11', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 17, 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:03:48');
INSERT INTO `gen_table_column` VALUES (198, '12', 'id', 'id', 'bigint(20) unsigned', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:04:10');
INSERT INTO `gen_table_column` VALUES (199, '12', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:04:10');
INSERT INTO `gen_table_column` VALUES (200, '12', 'dept_id', '部门ID', 'bigint(20)', 'Long', 'deptId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:04:10');
INSERT INTO `gen_table_column` VALUES (201, '12', 'name', '分类名称', 'varchar(100)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:04:10');
INSERT INTO `gen_table_column` VALUES (202, '12', 'icon', '分类图标', 'varchar(255)', 'String', 'icon', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'imageUpload', '', 5, 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:04:10');
INSERT INTO `gen_table_column` VALUES (203, '12', 'brief', '分类简介', 'varchar(255)', 'String', 'brief', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:04:11');
INSERT INTO `gen_table_column` VALUES (204, '12', 'sort', '排序', 'int(8)', 'Integer', 'sort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-08-04 10:57:07', '', '2022-08-04 11:04:11');
INSERT INTO `gen_table_column` VALUES (205, '12', 'del_flag', '删除状态(0未删除 1已删除)', 'tinyint(4)', 'Integer', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:04:11');
INSERT INTO `gen_table_column` VALUES (206, '12', 'status', '数据状态(0正常 1停用)', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'common_data_status', 9, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:04:11');
INSERT INTO `gen_table_column` VALUES (207, '12', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:04:11');
INSERT INTO `gen_table_column` VALUES (208, '12', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:04:11');
INSERT INTO `gen_table_column` VALUES (209, '12', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 12, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:04:11');
INSERT INTO `gen_table_column` VALUES (210, '12', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 13, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:04:11');
INSERT INTO `gen_table_column` VALUES (211, '12', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 14, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:04:11');
INSERT INTO `gen_table_column` VALUES (212, '13', 'id', 'id', 'bigint(20) unsigned', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:05:30');
INSERT INTO `gen_table_column` VALUES (213, '13', 'uid', '用户ID', 'bigint(20)', 'Long', 'uid', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:05:30');
INSERT INTO `gen_table_column` VALUES (214, '13', 'user_id', '系统用户ID', 'bigint(20)', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:05:30');
INSERT INTO `gen_table_column` VALUES (215, '13', 'dept_id', '系统部门ID', 'bigint(20)', 'Long', 'deptId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:05:30');
INSERT INTO `gen_table_column` VALUES (216, '13', 'msg_object', '消息对象(client客户 staff员工)', 'varchar(32)', 'String', 'msgObject', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'cms_msg_object', 5, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:05:30');
INSERT INTO `gen_table_column` VALUES (217, '13', 'msg_type', '消息类型', 'varchar(32)', 'String', 'msgType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'cms_msg_type', 6, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:05:30');
INSERT INTO `gen_table_column` VALUES (218, '13', 'msg_title', '消息标题', 'varchar(255)', 'String', 'msgTitle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-08-04 10:57:08', '', '2022-08-04 11:05:30');
INSERT INTO `gen_table_column` VALUES (219, '13', 'msg_content', '消息内容', 'varchar(1000)', 'String', 'msgContent', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 8, 'admin', '2022-08-04 10:57:09', '', '2022-08-04 11:05:31');
INSERT INTO `gen_table_column` VALUES (220, '13', 'msg_attach', '消息附件', 'varchar(1000)', 'String', 'msgAttach', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'fileUpload', '', 9, 'admin', '2022-08-04 10:57:09', '', '2022-08-04 11:05:31');
INSERT INTO `gen_table_column` VALUES (221, '13', 'send_user', '发送人', 'varchar(64)', 'String', 'sendUser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-08-04 10:57:09', '', '2022-08-04 11:05:31');
INSERT INTO `gen_table_column` VALUES (222, '13', 'send_time', '发送时间', 'datetime', 'Date', 'sendTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 11, 'admin', '2022-08-04 10:57:09', '', '2022-08-04 11:05:31');
INSERT INTO `gen_table_column` VALUES (223, '13', 'read_time', '阅读时间', 'datetime', 'Date', 'readTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 12, 'admin', '2022-08-04 10:57:09', '', '2022-08-04 11:05:31');
INSERT INTO `gen_table_column` VALUES (224, '13', 'is_read', '是否阅读(0否 1是)', 'tinyint(4)', 'Integer', 'isRead', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'common_yesno_status', 13, 'admin', '2022-08-04 10:57:09', '', '2022-08-04 11:05:31');
INSERT INTO `gen_table_column` VALUES (225, '13', 'del_flag', '删除状态(0未删除 1已删除)', 'tinyint(4)', 'Integer', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 14, 'admin', '2022-08-04 10:57:09', '', '2022-08-04 11:05:31');
INSERT INTO `gen_table_column` VALUES (226, '13', 'status', '数据状态(0正常 1停用)', 'tinyint(4)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'common_data_status', 15, 'admin', '2022-08-04 10:57:09', '', '2022-08-04 11:05:31');
INSERT INTO `gen_table_column` VALUES (227, '13', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 16, 'admin', '2022-08-04 10:57:09', '', '2022-08-04 11:05:31');
INSERT INTO `gen_table_column` VALUES (228, '13', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 17, 'admin', '2022-08-04 10:57:09', '', '2022-08-04 11:05:31');
INSERT INTO `gen_table_column` VALUES (229, '13', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 18, 'admin', '2022-08-04 10:57:09', '', '2022-08-04 11:05:31');
INSERT INTO `gen_table_column` VALUES (230, '13', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 19, 'admin', '2022-08-04 10:57:09', '', '2022-08-04 11:05:31');
INSERT INTO `gen_table_column` VALUES (231, '13', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 20, 'admin', '2022-08-04 10:57:09', '', '2022-08-04 11:05:31');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Blob类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '日历信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Cron类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '已触发的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '任务详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '暂停的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '调度器状态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '简单触发器的信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '同步机制的行锁表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '触发器详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-07-19 10:30:53', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-07-19 10:30:53', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-07-19 10:30:53', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaEnabled', 'true', 'Y', 'admin', '2022-07-19 10:30:53', '', NULL, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2022-07-19 10:30:53', '', NULL, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-07-19 10:30:43', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-07-19 10:30:43', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-07-19 10:30:43', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-07-19 10:30:43', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-07-19 10:30:43', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-07-19 10:30:43', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-07-19 10:30:43', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-07-19 10:30:43', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-07-19 10:30:43', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-07-19 10:30:43', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 132 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-07-19 10:30:53', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-07-19 10:30:53', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-07-19 10:30:53', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-07-19 10:30:53', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-07-19 10:30:53', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-07-19 10:30:53', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-07-19 10:30:53', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-07-19 10:30:53', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-07-19 10:30:53', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-07-19 10:30:53', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (103, 0, '正常', '0', 'common_data_status', NULL, 'primary', 'N', '0', 'admin', '2022-06-13 18:34:23', 'admin', '2022-06-13 21:57:22', NULL);
INSERT INTO `sys_dict_data` VALUES (104, 1, '停用', '1', 'common_data_status', NULL, 'danger', 'N', '0', 'admin', '2022-06-13 18:34:30', 'admin', '2022-06-13 21:57:31', NULL);
INSERT INTO `sys_dict_data` VALUES (105, 0, '未删除', '0', 'common_del_status', NULL, 'success', 'N', '0', 'admin', '2022-06-13 18:35:08', 'admin', '2022-07-23 15:10:19', NULL);
INSERT INTO `sys_dict_data` VALUES (106, 1, '已删除', '1', 'common_del_status', NULL, 'danger', 'N', '0', 'admin', '2022-06-13 18:35:16', 'admin', '2022-07-23 15:10:26', NULL);
INSERT INTO `sys_dict_data` VALUES (107, 0, '未审核', '0', 'common_check_status', NULL, 'info', 'N', '0', 'admin', '2022-06-13 18:35:45', 'admin', '2022-07-23 15:10:37', NULL);
INSERT INTO `sys_dict_data` VALUES (108, 1, '审核中', '1', 'common_check_status', NULL, 'primary', 'N', '0', 'admin', '2022-06-13 18:35:53', 'admin', '2022-07-23 15:10:43', NULL);
INSERT INTO `sys_dict_data` VALUES (109, 2, '审核通过', '2', 'common_check_status', NULL, 'success', 'N', '0', 'admin', '2022-06-13 18:36:04', 'admin', '2022-07-23 15:10:52', NULL);
INSERT INTO `sys_dict_data` VALUES (110, 3, '审核未通过', '3', 'common_check_status', NULL, 'danger', 'N', '0', 'admin', '2022-06-13 18:36:15', 'admin', '2022-07-23 15:10:56', NULL);
INSERT INTO `sys_dict_data` VALUES (111, 0, '否', '0', 'common_yesno_status', NULL, 'info', 'N', '0', 'admin', '2022-06-13 18:36:41', 'admin', '2022-07-23 15:11:33', NULL);
INSERT INTO `sys_dict_data` VALUES (112, 1, '是', '1', 'common_yesno_status', NULL, 'success', 'N', '0', 'admin', '2022-06-13 18:36:46', 'admin', '2022-07-23 15:11:19', NULL);
INSERT INTO `sys_dict_data` VALUES (113, 0, '服务端', 'service', 'common_data_source', NULL, 'default', 'N', '0', 'admin', '2022-07-23 15:12:24', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (114, 0, '客户端', 'client', 'common_data_source', NULL, 'default', 'N', '0', 'admin', '2022-07-23 15:12:32', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (115, 0, '阿里云', 'aliyun', 'sys_oss_service', NULL, 'default', 'N', '0', 'admin', '2022-07-23 17:19:30', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (116, 0, '七牛云', 'qiniu', 'sys_oss_service', NULL, 'default', 'N', '0', 'admin', '2022-07-23 17:19:47', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (117, 0, '其他', '0', 'cms_doc_type', NULL, 'default', 'N', '0', 'admin', '2022-06-22 23:51:59', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (118, 0, '协议', '1', 'cms_doc_type', NULL, 'default', 'N', '0', 'admin', '2022-06-22 23:52:08', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (119, 0, '资料', '2', 'cms_doc_type', NULL, 'default', 'N', '0', 'admin', '2022-06-22 23:52:15', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (120, 0, '功能异常', '0', 'cms_feedback_type', NULL, 'default', 'N', '0', 'admin', '2022-06-22 23:52:40', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (121, 0, '产品建议', '1', 'cms_feedback_type', NULL, 'default', 'N', '0', 'admin', '2022-06-22 23:52:46', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (122, 0, '违规举报', '2', 'cms_feedback_type', NULL, 'default', 'N', '0', 'admin', '2022-06-22 23:52:53', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (123, 0, '普通', '0', 'cms_feedback_handle_level', NULL, 'primary', 'N', '0', 'admin', '2022-06-22 23:53:23', 'admin', '2022-08-04 23:22:33', NULL);
INSERT INTO `sys_dict_data` VALUES (124, 0, '加急', '1', 'cms_feedback_handle_level', NULL, 'warning', 'N', '0', 'admin', '2022-06-22 23:53:29', 'admin', '2022-08-04 23:22:38', NULL);
INSERT INTO `sys_dict_data` VALUES (125, 0, '紧急', '2', 'cms_feedback_handle_level', NULL, 'danger', 'N', '0', 'admin', '2022-06-22 23:53:35', 'admin', '2022-08-04 23:22:42', NULL);
INSERT INTO `sys_dict_data` VALUES (126, 0, '待处理', '0', 'cms_feedback_status', NULL, 'info', 'N', '0', 'admin', '2022-06-22 23:54:03', 'admin', '2022-08-04 23:22:09', NULL);
INSERT INTO `sys_dict_data` VALUES (127, 0, '处理中', '1', 'cms_feedback_status', NULL, 'primary', 'N', '0', 'admin', '2022-06-22 23:54:11', 'admin', '2022-08-04 23:22:04', NULL);
INSERT INTO `sys_dict_data` VALUES (128, 0, '已完成', '2', 'cms_feedback_status', NULL, 'success', 'N', '0', 'admin', '2022-06-22 23:54:17', 'admin', '2022-08-04 23:22:14', NULL);
INSERT INTO `sys_dict_data` VALUES (129, 0, '客户', 'client', 'cms_msg_object', NULL, 'default', 'N', '0', 'admin', '2022-07-21 14:36:37', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (130, 0, '员工', 'staff', 'cms_msg_object', NULL, 'default', 'N', '0', 'admin', '2022-07-21 14:36:47', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (131, 0, '提醒消息', 'tip', 'cms_msg_type', NULL, 'default', 'N', '0', 'admin', '2022-07-21 14:37:20', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 114 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2022-07-19 10:30:51', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2022-07-19 10:30:51', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2022-07-19 10:30:51', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2022-07-19 10:30:51', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2022-07-19 10:30:51', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2022-07-19 10:30:52', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (11, '系统OSS服务商', 'sys_oss_service', '0', 'admin', '2022-07-23 17:16:48', '', NULL, '系统OSS服务商');
INSERT INTO `sys_dict_type` VALUES (102, '通用数据状态', 'common_data_status', '0', 'admin', '2022-06-13 18:34:06', '', NULL, '通用数据状态');
INSERT INTO `sys_dict_type` VALUES (103, '通用删除状态', 'common_del_status', '0', 'admin', '2022-06-13 18:34:59', '', NULL, '通用删除状态');
INSERT INTO `sys_dict_type` VALUES (104, '通用审核状态', 'common_check_status', '0', 'admin', '2022-06-13 18:35:30', '', NULL, '通用审核状态');
INSERT INTO `sys_dict_type` VALUES (105, '通用是否状态', 'common_yesno_status', '0', 'admin', '2022-06-13 18:36:33', '', NULL, '通用是否状态');
INSERT INTO `sys_dict_type` VALUES (106, '通用数据来源', 'common_data_source', '0', 'admin', '2022-07-23 15:12:07', '', NULL, '通用数据来源');
INSERT INTO `sys_dict_type` VALUES (108, 'CMS文档类型', 'cms_doc_type', '0', 'admin', '2022-06-22 23:51:48', '', NULL, 'CMS文档类型');
INSERT INTO `sys_dict_type` VALUES (109, 'CMS反馈类型', 'cms_feedback_type', '0', 'admin', '2022-06-22 23:52:29', '', NULL, 'CMS反馈类型');
INSERT INTO `sys_dict_type` VALUES (110, 'CMS反馈处理等级', 'cms_feedback_handle_level', '0', 'admin', '2022-06-22 23:53:14', '', NULL, 'CMS反馈处理等级');
INSERT INTO `sys_dict_type` VALUES (111, 'CMS反馈状态', 'cms_feedback_status', '0', 'admin', '2022-06-22 23:53:50', '', NULL, 'CMS反馈状态');
INSERT INTO `sys_dict_type` VALUES (112, 'CMS消息对象', 'cms_msg_object', '0', 'admin', '2022-07-21 14:36:21', 'admin', '2022-07-21 14:36:28', NULL);
INSERT INTO `sys_dict_type` VALUES (113, 'CMS消息类型', 'cms_msg_type', '0', 'admin', '2022-07-21 14:36:59', 'admin', '2022-07-21 14:37:05', NULL);

-- ----------------------------
-- Table structure for sys_group_category
-- ----------------------------
DROP TABLE IF EXISTS `sys_group_category`;
CREATE TABLE `sys_group_category`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `category_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '数据组名称',
  `category_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '数据提示',
  `category_key` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '数据字段',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '数据组字段',
  `category_type` tinyint(4) NULL DEFAULT 0 COMMENT '分类类型(0总后台配置)',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '数据状态',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '组合数据分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_group_category
-- ----------------------------
INSERT INTO `sys_group_category` VALUES (1, '首页轮播图', '首页轮播图', 'index_banner', '[{\"name\":\"名称\",\"field\":\"name\",\"type\":\"input\",\"params\":\"\"},{\"name\":\"图片\",\"field\":\"img\",\"type\":\"imageUpload\",\"params\":\"\"},{\"name\":\"跳转链接\",\"field\":\"url\",\"type\":\"input\",\"params\":\"\"}]', 0, 0, 0, '', '2022-08-01 23:30:57', '', NULL);
INSERT INTO `sys_group_category` VALUES (2, '首页菜单', '首页菜单', 'index_menu', '[{\"name\":\"菜单名称\",\"field\":\"name\",\"type\":\"input\",\"params\":\"\"},{\"name\":\"菜单图标\",\"field\":\"img\",\"type\":\"imageUpload\",\"params\":\"\"},{\"name\":\"跳转链接\",\"field\":\"url\",\"type\":\"input\",\"params\":\"\"}]', 0, 0, 0, '', '2022-08-07 20:25:20', '', '2022-08-07 20:26:30');

-- ----------------------------
-- Table structure for sys_group_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_group_data`;
CREATE TABLE `sys_group_data`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `category_id` bigint(20) NULL DEFAULT 0 COMMENT '分类ID',
  `group_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '数据组值',
  `group_key` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '数据字段',
  `sort` int(4) NULL DEFAULT 0 COMMENT '排序',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '数据状态',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '组合数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_group_data
-- ----------------------------
INSERT INTO `sys_group_data` VALUES (1, 1, '{\"name\":\"首图\",\"img\":\"https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/03/a7d62f2303bc46b7a6d0f5b64277940e.jpg\",\"url\":\"/\"}', 'index_banner', 1, 0, 0, '', '2022-08-03 10:20:38', '', NULL);
INSERT INTO `sys_group_data` VALUES (2, 1, '{\"name\":\"轮播图2\",\"img\":\"https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/07/d8769be05cec410bbcd804e4a82d2a22.jpg\",\"url\":\"/\"}', 'index_banner', 0, 0, 0, '', '2022-08-07 20:19:00', '', NULL);
INSERT INTO `sys_group_data` VALUES (3, 2, '{\"name\":\"反馈\",\"img\":\"https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/07/d50f83ea704448b6862196261bcc7150.png\",\"url\":\"/\"}', 'index_menu', 0, 0, 0, '', '2022-08-07 20:40:19', '', NULL);
INSERT INTO `sys_group_data` VALUES (4, 2, '{\"name\":\"排行榜\",\"img\":\"https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/07/55ffdf0f5def41ac8811f969cd2d6270.png\",\"url\":\"/\"}', 'index_menu', 0, 0, 0, '', '2022-08-07 20:40:32', '', NULL);
INSERT INTO `sys_group_data` VALUES (5, 2, '{\"name\":\"通知\",\"img\":\"https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/07/97111933cf0f4916970b36c36d6b64d6.png\",\"url\":\"/\"}', 'index_menu', 0, 0, 0, '', '2022-08-07 20:40:43', '', '2022-08-07 20:43:25');
INSERT INTO `sys_group_data` VALUES (6, 2, '{\"name\":\"客服\",\"img\":\"https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/07/b4befa4bd48c4ab49c96707a70663331.png\",\"url\":\"/\"}', 'index_menu', 0, 0, 0, '', '2022-08-07 20:40:52', '', NULL);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-07-19 10:30:53', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-07-19 10:30:53', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-07-19 10:30:53', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 142 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-07-19 11:25:46');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-19 11:27:12');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-22 10:35:41');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-22 11:37:05');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-22 14:47:38');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-22 14:47:45');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-22 14:47:48');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-07-23 11:06:09');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-07-23 11:06:13');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-23 11:06:21');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-23 15:02:00');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-23 17:10:15');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 11:59:00');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 11:59:04');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 14:31:17');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 15:44:01');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 23:52:53');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-25 14:25:00');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-25 16:20:22');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码已失效', '2022-08-01 15:50:04');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-01 15:50:07');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-08-03 10:17:45');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-03 10:17:46');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '用户不存在/密码错误', '2022-08-03 10:17:49');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误1次', '2022-08-03 10:17:49');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码已失效', '2022-08-03 10:18:00');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-03 18:51:15');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-03 18:51:21');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-04 10:24:24');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-04 15:41:25');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-04 21:12:58');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-05 13:39:21');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-08-05 23:48:06');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-05 23:48:09');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码已失效', '2022-08-06 16:48:09');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-08-06 16:48:11');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-08-06 16:48:13');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-06 16:48:20');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-07 10:31:11');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-07 17:56:48');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-08 09:55:04');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-08-08 14:59:15');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2081 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2022-07-19 10:30:44', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2022-07-19 10:30:44', '', NULL, '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2022-07-19 10:30:44', '', NULL, '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-07-19 10:30:44', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-07-19 10:30:44', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-07-19 10:30:44', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2022-07-19 10:30:44', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2022-07-19 10:30:44', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-07-19 10:30:44', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-07-19 10:30:44', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-07-19 10:30:44', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2022-07-19 10:30:44', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-07-19 10:30:45', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-07-19 10:30:45', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2022-07-19 10:30:45', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-07-19 10:30:45', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2022-07-19 10:30:45', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '缓存列表', 2, 6, 'cacheList', 'monitor/cache/list', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis-list', 'admin', '2022-07-19 10:30:45', '', NULL, '缓存列表菜单');
INSERT INTO `sys_menu` VALUES (115, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-07-19 10:30:45', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (116, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-07-19 10:30:45', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (117, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-07-19 10:30:45', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2022-07-19 10:30:45', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2022-07-19 10:30:45', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-07-19 10:30:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-07-19 10:30:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '日志导出', 500, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '账户解锁', 501, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:unlock', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 6, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-08-09 21:17:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-08-09 21:17:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-08-09 21:17:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-08-09 21:17:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-08-09 21:17:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-08-09 21:17:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-08-09 21:17:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, '对象存储', 1, 10, 'oss', 'system/oss/index', NULL, 1, 0, 'C', '0', '0', 'system:oss:list', 'guide', 'admin', '2022-07-24 16:04:12', 'admin', '2022-07-24 16:05:51', '对象存储菜单');
INSERT INTO `sys_menu` VALUES (2001, '对象存储查询', 2000, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:oss:query', '#', 'admin', '2022-07-24 16:04:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2002, '对象存储新增', 2000, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:oss:add', '#', 'admin', '2022-07-24 16:04:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2003, '对象存储修改', 2000, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:oss:edit', '#', 'admin', '2022-07-24 16:04:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2004, '对象存储删除', 2000, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:oss:remove', '#', 'admin', '2022-07-24 16:04:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, '对象存储导出', 2000, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:oss:export', '#', 'admin', '2022-07-24 16:04:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2018, '对象存储配置', 2000, 6, '', NULL, NULL, 1, 0, 'F', '0', '0', 'system:oss:config', '#', 'admin', '2022-07-25 15:17:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2019, '用户管理', 0, 4, 'ums', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'peoples', 'admin', '2022-08-01 17:20:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2020, '用户管理', 2019, 1, 'umsUser', 'ums/umsUser/index', NULL, 1, 0, 'C', '0', '0', 'ums:umsUser:list', 'peoples', 'admin', '2022-08-01 17:23:21', 'admin', '2022-08-01 17:24:42', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (2021, '用户管理查询', 2020, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'ums:umsUser:query', '#', 'admin', '2022-08-01 17:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2022, '用户管理新增', 2020, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'ums:umsUser:add', '#', 'admin', '2022-08-01 17:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2023, '用户管理修改', 2020, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'ums:umsUser:edit', '#', 'admin', '2022-08-01 17:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2024, '用户管理删除', 2020, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'ums:umsUser:remove', '#', 'admin', '2022-08-01 17:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2025, '用户管理导出', 2020, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'ums:umsUser:export', '#', 'admin', '2022-08-01 17:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2032, '组合数据', 1, 10, 'sysGroupCategory', 'system/sysGroupCategory/index', NULL, 1, 0, 'C', '0', '0', 'system:sysGroupCategory:list', 'example', 'admin', '2022-08-01 23:23:10', 'admin', '2022-08-01 23:28:39', '组合数据分类菜单');
INSERT INTO `sys_menu` VALUES (2033, '组合数据分类查询', 2032, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sysGroupCategory:query', '#', 'admin', '2022-08-01 23:23:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2034, '组合数据分类新增', 2032, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sysGroupCategory:add', '#', 'admin', '2022-08-01 23:23:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2035, '组合数据分类修改', 2032, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sysGroupCategory:edit', '#', 'admin', '2022-08-01 23:23:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2036, '组合数据分类删除', 2032, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sysGroupCategory:remove', '#', 'admin', '2022-08-01 23:23:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2037, '组合数据分类导出', 2032, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sysGroupCategory:export', '#', 'admin', '2022-08-01 23:23:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2038, '内容管理', 0, 5, 'cms', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'table', 'admin', '2022-08-04 10:58:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2039, '文章分类', 2038, 1, 'cmsArticleCategory', 'cms/cmsArticleCategory/index', NULL, 1, 0, 'C', '0', '0', 'cms:cmsArticleCategory:list', 'tree-table', 'admin', '2022-08-04 11:13:16', 'admin', '2022-08-04 12:09:42', '文章分类菜单');
INSERT INTO `sys_menu` VALUES (2040, '文章分类查询', 2039, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsArticleCategory:query', '#', 'admin', '2022-08-04 11:13:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2041, '文章分类新增', 2039, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsArticleCategory:add', '#', 'admin', '2022-08-04 11:13:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2042, '文章分类修改', 2039, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsArticleCategory:edit', '#', 'admin', '2022-08-04 11:13:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2043, '文章分类删除', 2039, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsArticleCategory:remove', '#', 'admin', '2022-08-04 11:13:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2044, '文章分类导出', 2039, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsArticleCategory:export', '#', 'admin', '2022-08-04 11:13:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2045, '文章管理', 2038, 2, 'cmsArticle', 'cms/cmsArticle/index', NULL, 1, 0, 'C', '0', '0', 'cms:cmsArticle:list', 'documentation', 'admin', '2022-08-04 11:13:23', 'admin', '2022-08-04 12:09:57', '文章菜单');
INSERT INTO `sys_menu` VALUES (2046, '文章查询', 2045, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsArticle:query', '#', 'admin', '2022-08-04 11:13:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2047, '文章新增', 2045, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsArticle:add', '#', 'admin', '2022-08-04 11:13:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2048, '文章修改', 2045, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsArticle:edit', '#', 'admin', '2022-08-04 11:13:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2049, '文章删除', 2045, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsArticle:remove', '#', 'admin', '2022-08-04 11:13:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2050, '文章导出', 2045, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsArticle:export', '#', 'admin', '2022-08-04 11:13:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2051, '平台文档', 2038, 5, 'cmsDocument', 'cms/cmsDocument/index', NULL, 1, 0, 'C', '0', '0', 'cms:cmsDocument:list', 'documentation', 'admin', '2022-08-04 11:13:28', 'admin', '2022-08-04 12:11:08', '平台相关文档菜单');
INSERT INTO `sys_menu` VALUES (2052, '平台相关文档查询', 2051, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsDocument:query', '#', 'admin', '2022-08-04 11:13:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2053, '平台相关文档新增', 2051, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsDocument:add', '#', 'admin', '2022-08-04 11:13:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2054, '平台相关文档修改', 2051, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsDocument:edit', '#', 'admin', '2022-08-04 11:13:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2055, '平台相关文档删除', 2051, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsDocument:remove', '#', 'admin', '2022-08-04 11:13:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2056, '平台相关文档导出', 2051, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsDocument:export', '#', 'admin', '2022-08-04 11:13:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2057, '反馈管理', 2038, 6, 'cmsFeedback', 'cms/cmsFeedback/index', NULL, 1, 0, 'C', '0', '0', 'cms:cmsFeedback:list', 'form', 'admin', '2022-08-04 11:13:34', 'admin', '2022-08-04 12:11:34', '用户反馈菜单');
INSERT INTO `sys_menu` VALUES (2058, '用户反馈查询', 2057, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsFeedback:query', '#', 'admin', '2022-08-04 11:13:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2059, '用户反馈新增', 2057, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsFeedback:add', '#', 'admin', '2022-08-04 11:13:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2060, '用户反馈修改', 2057, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsFeedback:edit', '#', 'admin', '2022-08-04 11:13:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2061, '用户反馈删除', 2057, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsFeedback:remove', '#', 'admin', '2022-08-04 11:13:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2062, '用户反馈导出', 2057, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsFeedback:export', '#', 'admin', '2022-08-04 11:13:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2063, '文档分类', 2038, 3, 'cmsHelpCategory', 'cms/cmsHelpCategory/index', NULL, 1, 0, 'C', '0', '0', 'cms:cmsHelpCategory:list', 'tree-table', 'admin', '2022-08-04 11:13:40', 'admin', '2022-08-04 12:12:11', '帮助文档分类菜单');
INSERT INTO `sys_menu` VALUES (2064, '帮助文档分类查询', 2063, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsHelpCategory:query', '#', 'admin', '2022-08-04 11:13:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2065, '帮助文档分类新增', 2063, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsHelpCategory:add', '#', 'admin', '2022-08-04 11:13:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2066, '帮助文档分类修改', 2063, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsHelpCategory:edit', '#', 'admin', '2022-08-04 11:13:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2067, '帮助文档分类删除', 2063, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsHelpCategory:remove', '#', 'admin', '2022-08-04 11:13:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2068, '帮助文档分类导出', 2063, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsHelpCategory:export', '#', 'admin', '2022-08-04 11:13:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2069, '帮助文档', 2038, 4, 'cmsHelp', 'cms/cmsHelp/index', NULL, 1, 0, 'C', '0', '0', 'cms:cmsHelp:list', 'documentation', 'admin', '2022-08-04 11:13:46', 'admin', '2022-08-04 12:10:54', '帮助文档菜单');
INSERT INTO `sys_menu` VALUES (2070, '帮助文档查询', 2069, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsHelp:query', '#', 'admin', '2022-08-04 11:13:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2071, '帮助文档新增', 2069, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsHelp:add', '#', 'admin', '2022-08-04 11:13:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2072, '帮助文档修改', 2069, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsHelp:edit', '#', 'admin', '2022-08-04 11:13:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2073, '帮助文档删除', 2069, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsHelp:remove', '#', 'admin', '2022-08-04 11:13:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2074, '帮助文档导出', 2069, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsHelp:export', '#', 'admin', '2022-08-04 11:13:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2075, '消息管理', 2038, 7, 'cmsMessage', 'cms/cmsMessage/index', NULL, 1, 0, 'C', '0', '0', 'cms:cmsMessage:list', 'message', 'admin', '2022-08-04 11:13:52', 'admin', '2022-08-04 12:12:00', '消息菜单');
INSERT INTO `sys_menu` VALUES (2076, '消息查询', 2075, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsMessage:query', '#', 'admin', '2022-08-04 11:13:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2077, '消息新增', 2075, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsMessage:add', '#', 'admin', '2022-08-04 11:13:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2078, '消息修改', 2075, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsMessage:edit', '#', 'admin', '2022-08-04 11:13:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2079, '消息删除', 2075, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsMessage:remove', '#', 'admin', '2022-08-04 11:13:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2080, '消息导出', 2075, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'cms:cmsMessage:export', '#', 'admin', '2022-08-04 11:13:52', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2022-07-19 10:30:54', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2022-07-19 10:30:54', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 237 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss`  (
  `oss_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文件名',
  `original_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '原文件名',
  `file_suffix` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文件后缀',
  `file_size` bigint(20) NULL DEFAULT 0 COMMENT '文件大小',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'URL地址',
  `source` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '来源(service后台 client客户端)',
  `service` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '服务商(aliyun阿里云 qiniu七牛云)',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '数据状态',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`oss_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '对象存储表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oss
-- ----------------------------
INSERT INTO `sys_oss` VALUES (2, 'RuoYiStaging/2022/07/25/59742b392ab848e89a4a98e9b8da6ebc.png', '微信图片_20220302105740.png', '.png', 31973, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/07/25/59742b392ab848e89a4a98e9b8da6ebc.png', 'service', 'aliyun', 0, 0, '', '2022-07-25 14:51:55', '', NULL);
INSERT INTO `sys_oss` VALUES (3, 'RuoYiStaging/2022/07/25/3784d028b6d64caf940d38f3c7b8eb98.png', '微信图片_20220302105740.png', '.png', 31973, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/07/25/3784d028b6d64caf940d38f3c7b8eb98.png', 'service', 'aliyun', 0, 0, '', '2022-07-25 14:53:50', '', NULL);
INSERT INTO `sys_oss` VALUES (4, 'RuoYiStaging/2022/07/25/a416fd7f8d084dc8984868e42ab6f308.png', '微信图片_20220302105740.png', '.png', 31973, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/07/25/a416fd7f8d084dc8984868e42ab6f308.png', 'service', 'aliyun', 0, 0, '', '2022-07-25 14:55:17', '', NULL);
INSERT INTO `sys_oss` VALUES (5, 'RuoYiStaging/2022/07/25/838e135b1efa4f53a65939cc36aa27bb.png', '微信图片_20220302105740.png', '.png', 31973, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/07/25/838e135b1efa4f53a65939cc36aa27bb.png', 'service', 'aliyun', 0, 0, '', '2022-07-25 15:06:22', '', NULL);
INSERT INTO `sys_oss` VALUES (6, 'RuoYiStaging/2022/07/25/8a23439378fa4685961ef768f4dcbb2e.png', '微信图片_20220302105740.png', '.png', 31973, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/07/25/8a23439378fa4685961ef768f4dcbb2e.png', 'service', 'aliyun', 0, 0, '', '2022-07-25 15:06:33', '', NULL);
INSERT INTO `sys_oss` VALUES (7, 'RuoYiStaging/2022/07/25/ae57226f4b6043c6938543d5ff234f38.pptx', '新建 PPTX 演示文稿.pptx', '.pptx', 29132, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/07/25/ae57226f4b6043c6938543d5ff234f38.pptx', 'service', 'aliyun', 0, 0, '', '2022-07-25 15:10:38', '', NULL);
INSERT INTO `sys_oss` VALUES (8, 'RuoYiStaging/2022/08/03/a7d62f2303bc46b7a6d0f5b64277940e.jpg', '图怪兽_6a87ae36041a814f80dc1d100d9c5850_60377.jpg', '.jpg', 47708, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/03/a7d62f2303bc46b7a6d0f5b64277940e.jpg', 'service', 'aliyun', 0, 0, '', '2022-08-03 10:20:27', '', NULL);
INSERT INTO `sys_oss` VALUES (9, 'RuoYiStaging/2022/08/04/78a58dddf8db4b6bb9aecd0eee9b8c1e.jpg', '图怪兽_6a87ae36041a814f80dc1d100d9c5850_60377.jpg', '.jpg', 47708, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/78a58dddf8db4b6bb9aecd0eee9b8c1e.jpg', 'service', 'aliyun', 0, 0, '', '2022-08-04 16:23:22', '', NULL);
INSERT INTO `sys_oss` VALUES (10, 'RuoYiStaging/2022/08/04/f5ec79ee9efa44b09e95f20147edefdf.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/f5ec79ee9efa44b09e95f20147edefdf.jpg', 'service', 'aliyun', 0, 0, '', '2022-08-04 16:27:43', '', NULL);
INSERT INTO `sys_oss` VALUES (11, 'RuoYiStaging/2022/08/04/c35aa0d08ad1416aa2040a739c65fe7f.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/c35aa0d08ad1416aa2040a739c65fe7f.jpg', 'service', 'aliyun', 0, 0, '', '2022-08-04 17:05:12', '', NULL);
INSERT INTO `sys_oss` VALUES (12, 'RuoYiStaging/2022/08/04/c047efe48ff44308bc417e5dd8c96e52.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/c047efe48ff44308bc417e5dd8c96e52.jpg', 'service', 'aliyun', 0, 0, '', '2022-08-04 17:14:28', '', NULL);
INSERT INTO `sys_oss` VALUES (13, 'RuoYiStaging/2022/08/04/a37cd6124eb549419d9c40a9f66d2f60.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/a37cd6124eb549419d9c40a9f66d2f60.jpg', 'service', 'aliyun', 0, 0, '', '2022-08-04 17:27:25', '', NULL);
INSERT INTO `sys_oss` VALUES (14, 'RuoYiStaging/2022/08/04/efac83d0c4674e9ba0a8da4bbdf0dcc6.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/04/efac83d0c4674e9ba0a8da4bbdf0dcc6.jpg', 'service', 'aliyun', 0, 0, '', '2022-08-04 17:33:00', '', NULL);
INSERT INTO `sys_oss` VALUES (15, 'RuoYiStaging/2022/08/05/0b10b028fb524d6c87d2459b3f40cd3b.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/0b10b028fb524d6c87d2459b3f40cd3b.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:05:21', '', NULL);
INSERT INTO `sys_oss` VALUES (16, 'RuoYiStaging/2022/08/05/357f65bb9cb247c593feb75844dd9fe7.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/357f65bb9cb247c593feb75844dd9fe7.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:26:11', '', NULL);
INSERT INTO `sys_oss` VALUES (17, 'RuoYiStaging/2022/08/05/9a6029ffdc29455eb40a7ba94fbe3a0c.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/9a6029ffdc29455eb40a7ba94fbe3a0c.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:27:58', '', NULL);
INSERT INTO `sys_oss` VALUES (18, 'RuoYiStaging/2022/08/05/6b1d357ea2684355990da09106656d9f.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/6b1d357ea2684355990da09106656d9f.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:29:14', '', NULL);
INSERT INTO `sys_oss` VALUES (19, 'RuoYiStaging/2022/08/05/dbe95728bc9b4715aa34ac537a1aa7d7.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/dbe95728bc9b4715aa34ac537a1aa7d7.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:29:22', '', NULL);
INSERT INTO `sys_oss` VALUES (20, 'RuoYiStaging/2022/08/05/64033cb93dc94f4698a9507c70d19474.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/64033cb93dc94f4698a9507c70d19474.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:30:36', '', NULL);
INSERT INTO `sys_oss` VALUES (21, 'RuoYiStaging/2022/08/05/213f75ea419b4658b0f27a78c83c9f73.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/213f75ea419b4658b0f27a78c83c9f73.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:31:04', '', NULL);
INSERT INTO `sys_oss` VALUES (22, 'RuoYiStaging/2022/08/05/f62f423c0f1e41b0b9c2845e761b82a1.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/f62f423c0f1e41b0b9c2845e761b82a1.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:31:21', '', NULL);
INSERT INTO `sys_oss` VALUES (23, 'RuoYiStaging/2022/08/05/4450316373ba4ec9bbb3231dc3e2da90.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/4450316373ba4ec9bbb3231dc3e2da90.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:31:24', '', NULL);
INSERT INTO `sys_oss` VALUES (24, 'RuoYiStaging/2022/08/05/16ba5d30e35c46e08cd4a80daeb3e955.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/16ba5d30e35c46e08cd4a80daeb3e955.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:31:32', '', NULL);
INSERT INTO `sys_oss` VALUES (25, 'RuoYiStaging/2022/08/05/221569216ab349f6959a32a4de18d706.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/221569216ab349f6959a32a4de18d706.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:32:01', '', NULL);
INSERT INTO `sys_oss` VALUES (26, 'RuoYiStaging/2022/08/05/16b1e3cd02a9408ab5241921821d8ec7.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/16b1e3cd02a9408ab5241921821d8ec7.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:34:16', '', NULL);
INSERT INTO `sys_oss` VALUES (27, 'RuoYiStaging/2022/08/05/f0064a9eb62043ab824f90b0461140ac.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/f0064a9eb62043ab824f90b0461140ac.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:36:54', '', NULL);
INSERT INTO `sys_oss` VALUES (28, 'RuoYiStaging/2022/08/05/bad92197f8cd474d9cb5a4b92be03e2b.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/bad92197f8cd474d9cb5a4b92be03e2b.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:38:09', '', NULL);
INSERT INTO `sys_oss` VALUES (29, 'RuoYiStaging/2022/08/05/c81e8d81a9864e7897befcc64773b368.jpg', '图怪兽_6a87ae36041a814f80dc1d100d9c5850_60377.jpg', '.jpg', 47708, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/c81e8d81a9864e7897befcc64773b368.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:38:11', '', NULL);
INSERT INTO `sys_oss` VALUES (30, 'RuoYiStaging/2022/08/05/1ebae82e1d4d483285c4aeb713ec39a5.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/1ebae82e1d4d483285c4aeb713ec39a5.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:38:19', '', NULL);
INSERT INTO `sys_oss` VALUES (31, 'RuoYiStaging/2022/08/05/f9f138eafcf94abb84110286e4fb7d14.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/f9f138eafcf94abb84110286e4fb7d14.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:38:21', '', NULL);
INSERT INTO `sys_oss` VALUES (32, 'RuoYiStaging/2022/08/05/65df2cca0e98449fba149367134fdd0a.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/65df2cca0e98449fba149367134fdd0a.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:39:39', '', NULL);
INSERT INTO `sys_oss` VALUES (33, 'RuoYiStaging/2022/08/05/3eed9a8a128b443f8dead6628c92a425.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/3eed9a8a128b443f8dead6628c92a425.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:40:40', '', NULL);
INSERT INTO `sys_oss` VALUES (34, 'RuoYiStaging/2022/08/05/686e08394cdb4b45a85d9642115b2750.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/686e08394cdb4b45a85d9642115b2750.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:46:16', '', NULL);
INSERT INTO `sys_oss` VALUES (35, 'RuoYiStaging/2022/08/05/f0850b2b6b314ffe89acdc50894ae462.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/f0850b2b6b314ffe89acdc50894ae462.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:46:04', '', NULL);
INSERT INTO `sys_oss` VALUES (36, 'RuoYiStaging/2022/08/05/a09ae33bbd4f4e65b1e5a3998ce83e8f.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/a09ae33bbd4f4e65b1e5a3998ce83e8f.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:47:02', '', NULL);
INSERT INTO `sys_oss` VALUES (37, 'RuoYiStaging/2022/08/05/25234c9352284628b987cfb00dfa5b77.jpg', 'logo.jpg', '.jpg', 18861, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/05/25234c9352284628b987cfb00dfa5b77.jpg', 'client', 'aliyun', 0, 0, '', '2022-08-05 23:47:04', '', NULL);
INSERT INTO `sys_oss` VALUES (38, 'RuoYiStaging/2022/08/07/f04a0e7b43b049b6bd7728d38b094624.jpg', '图怪兽_6a87ae36041a814f80dc1d100d9c5850_60377.jpg', '.jpg', 47708, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/07/f04a0e7b43b049b6bd7728d38b094624.jpg', 'service', 'aliyun', 0, 0, '', '2022-08-07 17:59:31', '', NULL);
INSERT INTO `sys_oss` VALUES (39, 'RuoYiStaging/2022/08/07/d8769be05cec410bbcd804e4a82d2a22.jpg', '图怪兽_6a87ae36041a814f80dc1d100d9c5850_60377.jpg', '.jpg', 47708, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/07/d8769be05cec410bbcd804e4a82d2a22.jpg', 'service', 'aliyun', 0, 0, '', '2022-08-07 20:18:57', '', NULL);
INSERT INTO `sys_oss` VALUES (40, 'RuoYiStaging/2022/08/07/d50f83ea704448b6862196261bcc7150.png', '反馈.png', '.png', 1246, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/07/d50f83ea704448b6862196261bcc7150.png', 'service', 'aliyun', 0, 0, '', '2022-08-07 20:40:16', '', NULL);
INSERT INTO `sys_oss` VALUES (41, 'RuoYiStaging/2022/08/07/55ffdf0f5def41ac8811f969cd2d6270.png', '排行榜.png', '.png', 1273, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/07/55ffdf0f5def41ac8811f969cd2d6270.png', 'service', 'aliyun', 0, 0, '', '2022-08-07 20:40:29', '', NULL);
INSERT INTO `sys_oss` VALUES (42, 'RuoYiStaging/2022/08/07/97111933cf0f4916970b36c36d6b64d6.png', '通知.png', '.png', 1293, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/07/97111933cf0f4916970b36c36d6b64d6.png', 'service', 'aliyun', 0, 0, '', '2022-08-07 20:40:41', '', NULL);
INSERT INTO `sys_oss` VALUES (43, 'RuoYiStaging/2022/08/07/b4befa4bd48c4ab49c96707a70663331.png', '客服.png', '.png', 1476, 'https://zouhuuoss.hnxiyun.com/RuoYiStaging/2022/08/07/b4befa4bd48c4ab49c96707a70663331.png', 'service', 'aliyun', 0, 0, '', '2022-08-07 20:40:49', '', NULL);

-- ----------------------------
-- Table structure for sys_oss_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss_config`;
CREATE TABLE `sys_oss_config`  (
  `oss_config_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `config_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '配置key',
  `access_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'accessKey',
  `secret_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '秘钥',
  `bucket_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '桶名称',
  `prefix` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '前缀',
  `endpoint` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '访问站点',
  `domain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '自定义域名',
  `region` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '域',
  `is_https` tinyint(4) NULL DEFAULT 0 COMMENT '是否https(0否 1是)',
  `ext1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '扩展字段',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态(0正常 1删除)',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '数据状态(0正常 1停用)',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`oss_config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '对象存储配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oss_config
-- ----------------------------
INSERT INTO `sys_oss_config` VALUES (1, 'aliyun', 'LTAI5tQYvvTAbGrM8MDezZFb', 'XJfMBaXqKWBnTGDkoDVIbddnbKOlDF', 'zouhuuoss', 'RuoYiStaging', 'oss-cn-beijing.aliyuncs.com', 'https://zouhuuoss.hnxiyun.com', '', 1, '', 0, 0, '', '2022-07-24 18:09:00', '', NULL, NULL);
INSERT INTO `sys_oss_config` VALUES (2, 'qiniu', '2QavSgl_Y88Hy-di2bX7PsytzvUnPj2NQpivlwFS', 'a08ZL3bUEmoCg2bJtU4FrHaAOxj51PYRsANxNjky', 'zouhuuoss', 'RuoYiStaging', 'oss-cn-beijing.aliyuncs.com', 'https://zouhuuoss.hnxiyun.com', '', 1, '', 0, 1, '', '2022-07-24 18:08:47', '', '2022-08-08 14:59:29', NULL);
INSERT INTO `sys_oss_config` VALUES (3, 'minio', 'LTAI5tQYvvTAbGrM8MDezZFb', 'XJfMBaXqKWBnTGDkoDVIbddnbKOlDF', 'zouhuuoss', 'RuoYiStaging', 'oss-cn-beijing.aliyuncs.com', 'https://zouhuuoss.hnxiyun.com', '', 1, '', 0, 1, '', '2022-07-24 18:08:52', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2022-07-19 10:30:44', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2022-07-19 10:30:44', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2022-07-19 10:30:44', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2022-07-19 10:30:44', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2022-07-19 10:30:44', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2022-07-19 10:30:44', 'admin', '2022-07-19 11:23:48', '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 117);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-08-08 14:59:15', 'admin', '2022-07-19 10:30:43', '', '2022-08-08 14:59:15', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-07-19 10:30:43', 'admin', '2022-07-19 10:30:43', '', NULL, '测试员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

-- ----------------------------
-- Table structure for ums_user
-- ----------------------------
DROP TABLE IF EXISTS `ums_user`;
CREATE TABLE `ums_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) NULL DEFAULT 0 COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT 0 COMMENT '部门ID',
  `wx_client_openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '微信公众号客户端openid',
  `wxm_client_openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '微信小程序客户端openid',
  `wxm_staff_openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '微信小程序员工端openid',
  `wx_unionid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '微信unionid',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `nickname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '昵称',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户头像',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `eamil` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '邮箱',
  `idcard` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '身份证号',
  `login_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP',
  `login_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录终端类型(minapp小程序 app APP)',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '登录时间',
  `last_login_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '上次登录IP',
  `last_login_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '上次登录终端类型',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '上次登录终端时间',
  `login_num` int(11) NULL DEFAULT 0 COMMENT '登录次数',
  `realname` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '真实姓名',
  `sex` tinyint(4) NULL DEFAULT 0 COMMENT '性别(0男 1女 2未知)',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '出生日期',
  `integral` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '积分',
  `balance` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '余额',
  `reg_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态(0未删除 1已删除)',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '数据状态(0正常 1停用)',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ums_user
-- ----------------------------
INSERT INTO `ums_user` VALUES (1, 0, 0, '', '', '', '', '18273881083', '123456', 'zouhuu', 'https://thirdwx.qlogo.cn/mmopen/vi_32/V88ayRzQjW3icG5opgJ5jFk7uPdr33ZLicicaicD39TRFnSvV1NJh3DIKaFFFZ5ee2l7lNuGlWq9nZAfSwUqe5zWpQ/132', '1827388', '', '', '', '', NULL, '', '', NULL, 0, '邹', 0, NULL, 0.00, 0.00, NULL, 0, 0, '', '', '2022-08-07 20:24:02', NULL, '');

SET FOREIGN_KEY_CHECKS = 1;
